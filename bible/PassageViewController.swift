//
//  PassageViewController.swift
//  bible
//
//  Created by BJ Peter DeLaCruz on 2/15/16.
//  Copyright © 2016 BJ Peter DeLaCruz. All rights reserved.
//

import UIKit

class PassageViewController: UIViewController {

    @IBOutlet weak var textField: UITextView!

    var controllerTitle: String?

    var verseCount: Int?

    var verses: [String: AnyObject]?

    var loadedFromJson: Bool?

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        textField.scrollRectToVisible(CGRectMake(0, 0, 1, 1), animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = controllerTitle!

        var passage = ""

        for index in 1...verseCount! {
            let verseNumber = String(index)
            if let _ = loadedFromJson {
                passage += verseNumber + ". " + (verses![String(index)] as! String) + "\n\n"
            } else {
                if let verse = verses![String(index)] as? [String: AnyObject] {
                    passage += verseNumber + ". " + (verse["verse"] as! String) + "\n\n"
                }
            }
        }

        textField.text = passage
    }

}