# Bible

## About

This repository contains code for my [Swift](https://developer.apple.com/swift)
application called **Bible**, which I developed for the
[App Design and Development for iOS](https://www.coursera.org/learn/ios-app-design-development)
course on [Coursera](https://www.coursera.org).
