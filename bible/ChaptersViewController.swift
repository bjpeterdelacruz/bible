//
//  DetailsViewController.swift
//  bible
//
//  Created by BJ Peter DeLaCruz on 2/15/16.
//  Copyright © 2016 BJ Peter DeLaCruz. All rights reserved.
//

import UIKit

class ChaptersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!

    var book: String?

    var chapter: String?

    var chapters: NSMutableArray = []

    let endpoint = "https://getbible.net/json?p="

    var json: [String: AnyObject]?

    var key: String?

    var loadedFromJson: Bool?

    /*
     * Downloads the contents of the book from the Internet via WiFi or cellular,
     * or loads the contents from a JSON file.
     *
     * If there is no Internet connection, the contents will be loaded from a JSON file.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.title = book!

        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Error: Unable to create Reachability")
            return
        }

        reachability.whenReachable = { reachability in
            dispatch_async(dispatch_get_main_queue()) {
                let wifi = Utils().isWiFiEnabled()
                let cellular = Utils().isCellularEnabled()

                if Utils().isMissingBook(self.book!) {
                    self.loadFromJsonFile()
                }
                else {
                    if reachability.isReachableViaWiFi() && wifi == true {
                        print("Downloading content using WiFi [" + self.book! + "]")
                        self.startDownloadTask()
                    } else if reachability.isReachableViaWWAN() && cellular != nil && cellular == true {
                        print("Downloading content using cellular [" + self.book! + "]")
                        self.startDownloadTask()
                    } else {
                        self.loadFromJsonFile()
                    }
                }
            }
        }

        reachability.whenUnreachable = { reachability in
            dispatch_async(dispatch_get_main_queue()) {
                print("Internet connection unavailable.")
                self.loadFromJsonFile()
            }
        }

        do {
            try reachability.startNotifier()
        } catch {
            print("Error: unable to start notifier")
        }
    }

    /*
     * Loads the contents of the book from a JSON file.
     */
    func loadFromJsonFile() {
        loadedFromJson = true
        print("Loading content from JSON file [" + book! + "]")

        let file = NSBundle.mainBundle().pathForResource("EntireBible-CPDV", ofType: "json")
        let contents = try? NSString(contentsOfFile: file!, encoding: NSUTF8StringEncoding)

        key = book!
        if let temp = Utils().splitString(key!) {
            if temp.count > 1 {
                key = temp[0] + "-" + temp[temp.count - 1]
            }
        }
        parseJson((contents?.dataUsingEncoding(NSUTF8StringEncoding))!)
        updateTable()
    }

    /*
     * Downloads the contents of the book from the Internet.
     *
     * If an error occurs, the contents will be loaded from a JSON file.
     */
    func startDownloadTask() {
        let newEndpoint = endpoint + book!.stringByReplacingOccurrencesOfString(" ", withString: "")
        guard let url = NSURL(string: newEndpoint) else {
            print("Error: URL cannot be created")
            loadFromJsonFile()
            return
        }
        let request = NSURLRequest(URL: url)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
            guard let _ = data else {
                print("Error: no data was received")
                self.loadFromJsonFile()
                return
            }
            guard error == nil else {
                print(error)
                self.loadFromJsonFile()
                return
            }

            var dataString = String(data: data!, encoding: NSUTF8StringEncoding)
            dataString = Utils().removeCharactersAtBeginning(dataString!, count: 1) // Remove (
            dataString = Utils().removeCharactersAtEnd(dataString!, count: 2) // Remove );

            self.key = "book"
            self.parseJson(dataString!.dataUsingEncoding(NSUTF8StringEncoding)!)
            self.updateTable()
        })
        task.resume()
    }

    func parseJson(data: NSData) {
        do {
            json = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject]
            if let chapterNumbers = json![key!] as? [String: AnyObject] {
                for var index = 1; index <= chapterNumbers.count; index++ {
                    self.chapters.addObject(String(index))
                }
            }
        } catch {
            print(error)
        }
    }

    func updateTable() {
        dispatch_async(dispatch_get_main_queue()) {
            self.tableView.reloadData()
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chapters.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")!
        
        cell.textLabel?.text = chapters[indexPath.row] as? String
        
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        chapter = chapters[indexPath.row] as? String
        self.performSegueWithIdentifier("showPassageSegue", sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let passageViewController = segue.destinationViewController as! PassageViewController
        passageViewController.controllerTitle = book! + " " + chapter!
        if let chapterNumbers = self.json![key!] as? [String: AnyObject] {
            if let _ = loadedFromJson {
                if let verses = chapterNumbers[chapter!] as? [String: AnyObject] {
                    passageViewController.loadedFromJson = true
                    passageViewController.verseCount = verses.count
                    passageViewController.verses = verses
                }
            } else {
                if let chapter = chapterNumbers[chapter!] as? [String: AnyObject] {
                    if let verses = chapter["chapter"] as? [String: AnyObject] {
                        passageViewController.verseCount = verses.count
                        passageViewController.verses = verses
                    }
                }
            }
        }
    }

}