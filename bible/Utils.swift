//
//  Utils.swift
//  bible
//
//  Created by BJ Peter DeLaCruz on 2/18/16.
//  Copyright © 2016 BJ Peter DeLaCruz. All rights reserved.
//

import UIKit

class Utils {

    func isWiFiEnabled() -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        var use_wifi = defaults.objectForKey("use_wifi")
        let use_cellular = defaults.objectForKey("use_cellular")
        if use_wifi == nil && use_cellular == nil {
            use_wifi = defaults.objectForKey("wifi")
        }
        return use_wifi as! Bool
    }

    func isCellularEnabled() -> Bool? {
        let defaults = NSUserDefaults.standardUserDefaults()
        let use_cellular = defaults.objectForKey("use_cellular")
        return use_cellular as? Bool
    }

    func splitString(string: String) -> Array<String>? {
        if let _ = string.rangeOfCharacterFromSet(NSCharacterSet.whitespaceCharacterSet()) {
            return string.characters.split{$0 == " "}.map(String.init)
        }
        return nil
    }

    func removeCharactersAtBeginning(string: String, count: Int) -> String {
        return (string as NSString).substringFromIndex(count)
    }

    func removeCharactersAtEnd(string: String, count: Int) -> String {
        return (string as NSString).substringToIndex(string.characters.count - count)
    }

    func isMissingBook(book: String) -> Bool {
        return book == "Tobit" || book == "Judith" || book == "1 Maccabees" || book == "2 Maccabees" ||
            book == "Wisdom" || book == "Sirach" || book == "Baruch"
    }

}
